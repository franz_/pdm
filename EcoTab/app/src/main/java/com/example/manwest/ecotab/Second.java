package com.example.manwest.ecotab;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class Second extends AppCompatActivity {
    private Spinner sp;
    private List<String> ls;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        muestra();
    }
    //agregando el titulo de una lista desplegable con
    //los centros de reciclaje mas cercanos
    private void muestra(){
        sp = (Spinner) findViewById(R.id.spinner);
        ls = new ArrayList<String>();
        sp = (Spinner) this.findViewById(R.id.spinner);
        ls.add("Centros Cercanos");
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ls);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(adaptador);
    }
}
