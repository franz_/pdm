package com.example.manwest.ecotab;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    //el metodo localizar() es para que nos localice
    //y luego nos muestre una nueva activity con los
    //centros de reciclaje mas cercanos a nuestra posicion
    public void localizar(View v){
        Intent intent = new Intent(MainActivity.this,Second.class);
        startActivity(intent);
    }
    //el metodo gps() permitira visualizar en el mapa
    //a todos los centros de reciclaje
    public void gps(View v){
        String coord = "geo:-12.0240167,-77.0503328";//ubicacion de la UNI
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(coord));
        startActivity(intent);
    }
}
