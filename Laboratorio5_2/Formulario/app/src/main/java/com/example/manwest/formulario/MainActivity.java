package com.example.manwest.formulario;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    private Button btnVolver;
    private Button btnSalvar;
    private ImageButton imagenBtn;
    private EditText edittext;
    private CheckBox checkbox;
    private RadioGroup radioGroup;
    private RadioButton radio_red;
    private RadioButton radio_blue;
    private ToggleButton togglebutton;
    private RatingBar ratingbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpViews();
    }

    private void setUpViews() {
        imagenBtn = (ImageButton) findViewById(R.id.Boton3);
        btnVolver = (Button) findViewById(R.id.Boton1);
        btnSalvar = (Button) findViewById(R.id.Boton2);
        edittext = (EditText) findViewById(R.id.Texto3);
        checkbox = (CheckBox) findViewById(R.id.Box1);
        radioGroup = (RadioGroup) findViewById(R.id.Radio1);
        radio_red = (RadioButton) findViewById(R.id.Radio2);
        radio_blue = (RadioButton) findViewById(R.id.Radio3);
        togglebutton = (ToggleButton) findViewById(R.id.Toggle1);
        ratingbar = (RatingBar) findViewById(R.id.Rating1);

        imagenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this,
                                        "ImageButton seleccionado",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }).start();
            }
        });
        btnVolver.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                finish();
            }
        });
    }
    public void sendClick(View v){
        // Perform action on clicks
        String allText = new String("campo:" + edittext.getText());
        allText = allText + ":checkbox:";
        if (checkbox.isChecked()) {
            allText = allText + "Checked:";
        } else {
            allText = allText + "Not Checked:";
        }
        allText = allText + ":toggle:";
        if (togglebutton.isChecked()) {
            allText = allText + "Checked:";
        } else {
            allText = allText + "Not Checked:";
        }
        allText = allText + "radios:rojo:";
        String redtext = "";
        if (radio_red.isChecked()) {
            redtext = "pulsado:";
        } else {
            redtext = "no pulsado:";
        }
        allText = allText + redtext;
        allText = allText + "azul";
        String bluetext = "";
        if (radio_blue.isChecked()) {
            bluetext = "pulsado:";
        } else {
            bluetext = "no pulsado:";
        }
        allText = allText + bluetext;
        allText = allText + "rating:";
        float f = ratingbar.getRating();
        allText = allText + Float.toString( f ) + ":";
        Log.d("app", allText);
        Toast.makeText(this, allText, Toast.LENGTH_LONG).show();
    }
    public void checkBoxClick(View v) {
        String text = "";
        if (checkbox.isChecked()) {
            text = "Selected";
            btnSalvar.setEnabled(true);
            Toast.makeText(this,"Ya puedes Salvar", Toast.LENGTH_LONG).show();
            btnVolver.setEnabled(true);
        } else {
            btnVolver.setEnabled(false);
            Toast.makeText(this, "Hasta que no marques la casilla no podrás salvar",
                    Toast.LENGTH_LONG).show();
            text = "Not selected";
        }
        Toast.makeText(this,text, Toast.LENGTH_SHORT).show();
    }
    public void checkRadioGroup(View v){
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        String cad = "";
                        if (radio_blue.isChecked()) {
                            cad = "azul";
                        } else if (radio_red.isChecked()) {
                            cad = "rojo";
                        }
                        Toast.makeText(MainActivity.this,
                                "RadioButton " + cad + " seleccionado",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }
    public void checkTaggleButton(View v){
        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        String cad="";
                        if(togglebutton.isChecked()){
                            cad="Encendido";
                        }
                        else{
                            cad="Apagado";
                        }
                        Toast.makeText(MainActivity.this,
                                "Vibrador "+cad, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }
}
