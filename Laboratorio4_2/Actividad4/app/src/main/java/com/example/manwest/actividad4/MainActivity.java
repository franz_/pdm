package com.example.manwest.actividad4;

import android.app.AlarmManager;
import android.content.Intent;
import android.net.Uri;
import android.provider.AlarmClock;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void abrirPagina(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://www.uni.edu.pe/"));
        startActivity(intent);
    }
    public void marcar(View view){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        startActivity(intent);
    }
    public void llamar(View view){
        String tfno = "948032831"; //mi otro numero
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + tfno));
        startActivity(intent);
    }
    public void agregar(View view){
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
        startActivity(intent);
    }
    public void alarma(View view){
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);
        intent.putExtra(AlarmClock.EXTRA_MESSAGE, "New Alarm");
        intent.putExtra(AlarmClock.EXTRA_HOUR, 11);
        intent.putExtra(AlarmClock.EXTRA_MINUTES, 20);
        startActivity(intent);
    }
    public void enviar(View view){
        String mail = "franz.maguina.a@uni.pe";//mi correo
        String subject = "Enviado desde Actividad4";
        String envio = "Hola, q haciendo? xD";
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto",mail,null));
        intent.putExtra(Intent.EXTRA_SUBJECT,subject);
        startActivity(Intent.createChooser(intent, envio));
    }
    public void foto(View view){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivity(intent);
    }
    public void video(View view){
        Intent intent = new Intent(MediaStore.INTENT_ACTION_VIDEO_CAMERA);
        startActivity(intent);
    }
    public void gps(View view){
        String coord = "geo:-12.0240167,-77.0503328";//ubicacion de la UNI
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(coord));
        startActivity(intent);
    }
    public void compartir(View view){
        String text = "comparte!";
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT,text);
        intent.setType("text/plain");
        startActivity(intent);
    }

}
